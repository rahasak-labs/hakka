# hAkka

1. Connect cassandra and elasticsearch with akka streams. Read more from [here](https://medium.com/@itseranga/index-cassandra-data-on-elasticsearch-with-akka-streams-6e9298a75190).

2. End to end streaming with kafka and cassandra. Read more from [here](https://medium.com/@itseranga/end-to-end-streaming-from-kafka-to-cassandra-447d0e6ba25a)

2. Hacking with akka streams. Read more from [here](https://medium.com/@itseranga/akka-streams-hacks-33661b8f7f2d)