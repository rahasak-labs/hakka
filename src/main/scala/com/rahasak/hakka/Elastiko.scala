package com.rahasak.hakka

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.cassandra.scaladsl.CassandraSource
import akka.stream.alpakka.elasticsearch.scaladsl.ElasticsearchSink
import akka.stream.alpakka.elasticsearch.{ElasticsearchWriteSettings, RetryAtFixedRate, WriteMessage}
import com.datastax.driver.core.{Cluster, SimpleStatement}
import org.apache.http.HttpHost
import org.elasticsearch.client.RestClient

import scala.concurrent.duration._
import scala.util.{Failure, Success}

object Elastiko extends App {

  // cassandra keeps Account Row objects
  // elasticsearch index Account object types
  case class Account(id: String, amount: Int, bank: String)

  // we need to actor system and actor materializer
  // to execute(materialize in akka stream terms) we need materializer.
  // materializer is a special tool that runs streams
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val context = system.dispatcher

  // cassandra cluster
  val cluster = Cluster.builder().addContactPoint("localhost").withPort(9042).build()
  implicit val session = cluster.connect("mystiko")

  // data source with select query
  // we fetching 200 rows per batch
  val statement = new SimpleStatement(s"SELECT * FROM mystiko.accounts").setFetchSize(200)
  val source = CassandraSource(statement)

  // elastic cluster and
  implicit val client = RestClient.builder(new HttpHost("localhost", 9200)).build()

  // elasticsearch write settings
  val sinkSettings = ElasticsearchWriteSettings()
    .withBufferSize(10)
    .withVersionType("internal")
    .withRetryLogic(RetryAtFixedRate(maxRetries = 5, retryInterval = 1.second))

  // elasticsearch sink write data to elastic index as JSON
  // so we need to convert accounts to json
  import spray.json.DefaultJsonProtocol._
  implicit val format = jsonFormat3(Account)

  // sink with
  //  1.index
  //  2. type
  //  3. settings
  val sink = ElasticsearchSink.create[Account]("accounts", "account", sinkSettings)

  // stream from source to sink
  // goes through two flows
  val stream = source
    .map(r => Account(r.getString("id"), r.getInt("amount"), r.getString("bank")))
    .map(a => WriteMessage.createIndexMessage(a.id, a))
    .runWith(sink)

  // listen for stream complete
  stream onComplete {
    case Success(Done) =>
      println("done stream")
    case Failure(e) =>
      e.printStackTrace()
  }

}
