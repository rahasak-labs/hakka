package com.rahasak.hakka

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.alpakka.cassandra.scaladsl.CassandraSource
import akka.stream.scaladsl.{Balance, Broadcast, Concat, Flow, GraphDSL, Partition, RunnableGraph, Sink, Source}
import akka.{Done, NotUsed}
import com.datastax.driver.core.{Cluster, SimpleStatement}

import scala.concurrent.Future
import scala.concurrent.duration._


object Hakka extends App {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  import system.dispatcher

  //combine()
  //combine2()
  //broadcast()
  //balance()
  //groupBy()
  //filter()
  //mapSync()
  //mapAsync()
  async()

  def filter() = {
    // filter odd numbers
    Source(1 to 20)
      .filter(_ % 2 == 0)
      .runWith(Sink.foreach(println))
  }

  def combine1() = {
    // cassandra cluster
    val cluster = Cluster.builder().addContactPoint("localhost").withPort(9042).build()
    implicit val session = cluster.connect("mystiko")

    // two data source from cassandra
    //  1. accounts
    //  2. users
    val sourceAccount = CassandraSource(new SimpleStatement(s"SELECT * FROM mystiko.accounts").setFetchSize(200))
    val sourceUser = CassandraSource(new SimpleStatement(s"SELECT * FROM mystiko.users").setFetchSize(200))

    // combine two source streams
    Source.combine(sourceAccount, sourceUser)(Concat(_))
      .runWith(Sink.foreach(println))
  }

  def combine2(): Future[Done] = {
    // streams
    val s1 = Source(List(1, 2, 3, 7, 10))
    val s2 = Source(List(10, 20, 30, 15, 19))

    // filter flow
    val filterFlow: Flow[Int, Int, NotUsed] = Flow[Int]
      .filter(_ > 2)

    // multiply flow
    val multiplyFlow: Flow[Int, Int, NotUsed] = Flow[Int]
      .map(_ * 2)

    // to string flow
    val toStringFlow: Flow[Int, String, NotUsed] = Flow[Int]
      .map(_.toString)

    // combine streams and apply flows
    Source.combine(s1, s2)(Concat(_))
      .via(filterFlow)
      .via(multiplyFlow)
      .via(toStringFlow)
      .runWith(Sink.foreach(println))
  }

  def grouped() = {
    // take 10 element batches from source and process
    Source(1 to 100)
      .grouped(10)
      .runWith(Sink.foreach(println))
  }

  def groupWithin() = {
    Source
      .tick(0.millis, 10.millis, ())
      .groupedWithin(100, 100.millis)
      .runWith(Sink.foreach(println))
  }

  def mapSync() = {
    // user objects
    case class User(id: Int, name: String, age: Int)

    // mock function to query user from db, blocking
    def querySync(id: Int): User = {
      println(s"start query - $id")
      Thread.sleep(1000)
      println(s"finish query - $id")
      User(1, s"user$id", 30)
    }

    val source = Source(List(3, 2, 5, 7, 8))

    // query flow sync
    val queryFlowSync: Flow[Int, User, NotUsed] = Flow[Int]
      .map(i => querySync(i))

    // filter name flow
    val nameFlow: Flow[User, String, NotUsed] = Flow[User]
      .map(u => u.name)

    // graph
    source
      .via(queryFlowSync)
      .via(nameFlow)
      .runWith(Sink.foreach(println))
  }

  def mapAsync() = {
    // user objects
    case class User(id: Int, name: String, age: Int)

    // mock function to query user from db, non blocking
    def queryAsync(id: Int): Future[User] = {
      Future {
        println(s"start query - $id")
        Thread.sleep(1000)
        println(s"finish query - $id")
        User(1, s"user$id", 30)
      }
    }

    val source = Source(List(3, 2, 5, 7, 8))

    // query flow async
    val queryFlowAsync: Flow[Int, User, NotUsed] = Flow[Int]
      .mapAsync(5)(i => queryAsync(i))

    // filter name flow
    val nameFlow: Flow[User, String, NotUsed] = Flow[User]
      .map(u => u.name)

    // graph
    source
      .via(queryFlowAsync)
      .via(nameFlow)
      .runWith(Sink.foreach(println))
  }

  def async() = {
    // user objects
    case class User(id: Int, name: String, age: Int)

    // mock function to query user from db, blocking
    def querySync(id: Int): Future[User] = {
      Future {
        println(s"start query - $id")
        Thread.sleep(1000)
        println(s"finish query - $id")
        User(1, s"user$id", 30)
      }
    }

    val source = Source(1 to 20)

    // query flow sync
    val queryFlowSync = Flow[Int]
      .map(i => querySync(i))

    // graph
    source
      .via(queryFlowSync)
      .runWith(Sink.ignore)
  }

  def partition() = {
    // source with list of responses
    // one response contains (Arg, Result)
    case class Arg(name: String)
    case class Result(name: String, rating: Int)
    val resp = List(
      (Arg("scala"), Option(Result("scala", 4))),
      (Arg("golang"), Option(Result("golang", 6))),
      (Arg("java"), None),
      (Arg("haskell"), Option(Result("haskell", 5))),
      (Arg("erlang"), Option(Result("erlang", 5)))
    )
    val source = Source(resp)

    // flows to handle
    //  1. when having Result, handle Rating
    //  2. when None Result, handle Arg
    val ratingFlow = Flow[(Arg, Option[Result])].map(_._2)
    val argFlow = Flow[(Arg, Option[Result])].map(_._1)

    // sinks to handle
    //  1. when having result, handle Rating
    //  2. when None result, handle Arg
    val ratingSink = Sink.foreach[Option[Result]](r => println(s"Rating found, name - ${r.get.name}, rating - ${r.get.rating}"))
    val argSink = Sink.foreach[Arg](r => println(s"Rating not found, arg - ${r.name}"))

    // graph
    val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      // partition response with rating and response without rating
      val partition = b.add(Partition[(Arg, Option[Result])](2, r => if (r._2.isDefined) 0 else 1))

      // source directs to partition
      source ~> partition.in

      // flow stream in two flows
      //  1. with rating
      //  2. without rating
      partition.out(0) ~> ratingFlow ~> ratingSink
      partition.out(1) ~> argFlow ~> argSink

      ClosedShape
    })
    graph.run()
  }

  def broadcast() = {
    // source with list, list element contains[(String, Int)]
    val langs: List[(String, Int)] = List(("Scala", 5), ("Golang", 8), ("Haskell", 7), ("Erlang", 5))
    val source = Source(langs)

    // two flows
    //  1. to extract name from list element[(String, Int)]
    //  2. to extract rate from list element[(String, Int)]
    val nameFlow: Flow[(String, Int), String, NotUsed] = Flow[(String, Int)].map(_._1)
    val rateFlow = Flow[(String, Int)].map(_._2)

    // two sinks
    //  1. print name[String]
    //  2. print rate[Int]
    val nameSink = Sink.foreach[String](p => println(s"Name - $p"))
    val rateSink = Sink.foreach[Int](p => println(s"Rate - $p"))

    // graph
    val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      // broadcaster to broadcast list elements[(String, Int)] with two streams
      val broadcaster = b.add(Broadcast[(String, Int)](2))

      // source directs to broadcaster
      source ~> broadcaster.in

      // broadcast list element with two streams
      //  1. to name sink
      //  2. to rate sink
      broadcaster.out(0) ~> nameFlow ~> nameSink
      broadcaster.out(1) ~> rateFlow ~> rateSink

      ClosedShape
    })
    graph.run()
  }

  def balance() = {
    // source with list
    val source = Source(1 to 10)

    // two flows
    //  1. flow with back pressure
    //  2. flow without back pressure
    val backPressureFlow = Flow[Int].throttle(1, 1.second, 1, ThrottleMode.shaping).map(_ * 2)
    val normalFlow = Flow[Int].map(_ * 2)

    // two sinks
    //  1. print elements coming from back-pressured flow
    //  2. print elements coming from normal flow
    val backPressureSink = Sink.foreach[Int](p => println(s"Coming from back-pressured flow - $p"))
    val normalSink = Sink.foreach[Int](p => println(s"Coming from normal flow - $p"))

    // graph
    val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      // balancer to balance source list elements with two streams
      val balancer = b.add(Balance[Int](2))

      // source directs to balancer
      source ~> balancer

      // balance list elements in two streams
      //  1. back pressure flow
      //  2. normal flow
      balancer.out(0) ~> backPressureFlow ~> backPressureSink
      balancer.out(1) ~> normalFlow ~> normalSink

      ClosedShape
    })
    graph.run()
  }

  def groupBy() = {
    // source partition into sub streams based on modules 3 operator (_ % 3)
    // max sub stream size is 4
    // we handle sub streams asynchronously and merge them together at the end
    Source(1 to 10)
      .groupBy(4, _ % 3)
      .map(_ * 2)
      .async
      .mergeSubstreams
      .runWith(Sink.foreach(println))
  }

}
